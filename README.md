# KumuluzEE Microservices in Node.js and Java 

The goal of this tutorial is to implement the healthcheck module is a simple express-based http server. 
The whole documentation is 

## Implementation in 4 steps.
To implement the server we only need 4 steps. 

Step 1.

We need a configuration library, to load the default.json (or any other) configuration. Import it and load the config.

Step 2. 

Expose the configuration in global namespace so it is accessible in the module at all times in case of any changes (for example: change a API endpoint URL at runtime)

Step 3.

Create a Kumuluz healthcheck instance. For this you need to import the module, and optionally pass it custom logging functions (useful when using a logging server). 

Step 4. 

Expose "/health" path (default path for Prometheus, Kubernetes) and pass it the Kumuluz healthcheck instance. You can also expose another path if you like, but then you have to configure the checking services as well.


```
const express = require('express');
let app = express();
const port = process.env.PORT || 8080;

// ------ Start of initialisation -------
Step 1.
// import any configuration library
let config = require('config');
// get configuration file
options = config.get("healthOptions");

Step 2.
// export it so it is accessible by library
module.exports = {
   kumuluzHealthOptions: () => {
       return options
   }
};

Step 3. 
// import the library and give it custom logging functions if needed
let kumuluzInstance = require("@kumuluz/kumuluzee-health")({
   log: console.log, error: console.error, customAPI: () => {
       console.log("Custom API message")
   }
});

Step 4.
// expose kumuluzee healthcheck instance
app.use("/health", kumuluzInstance);

// ------ The library is now configured -------

app.listen(port, function () {
   console.log(`Sample server is running on port:`, port);
});
   
```

