const express = require('express');
let app = express();
const port = process.env.PORT || 8080;

// ------ Start of initialisation -------
// import any configuration library
let config = require('config');
// get configuration file
options = config.get("healthOptions");

// export it so it is accessible by library
module.exports = {
    kumuluzHealthOptions: () => {
        return options
    }
};

// import the library and give it custom logging functions if needed
let kumuluzInstance = require("@kumuluz/kumuluzee-health")({
    log: console.log, error: console.error, customAPI: () => {
        console.log("Custom API message")
    }
});

// expose kumuluzee healthcheck instance
app.use("/health", kumuluzInstance);

// ------ The library is now configured -------

app.listen(port, function () {
    console.log(`Sample server is running on port:`, port);
});

